from django.conf.urls import url

import clients.views as views

urlpatterns = [
    url(r'^$', views.list_view),
    url(r'^(?P<script_name>\w+\.(js|css))', views.get_js),
    url(r'^rows', views.get_rows),
]