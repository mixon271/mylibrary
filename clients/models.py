from django.db import models

from core.models import Person


class Client(Person):
    passport = models.CharField(max_length=10, null=True)
    library_card = models.CharField(max_length=8)
    valid = models.BooleanField(default=True)

    class Meta:
        db_table = 'clients'
        ordering = ['fname', 'iname']

    def get_passport_series(self):
        if self.passport is None:
            return None
        return ' '.join((self.passport[:2], self.passport[2:4]))

    def get_passport_number(self):
        if self.passport is None:
            return None
        return self.passport[4:]
