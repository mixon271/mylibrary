import json

from core.api import get_rows_getter, get_list_view, get_js_getter
from clients.models import Client


model = Client
template_name = 'clients_list.html'

columns = [
    {
        'attribute': 'fname',
        'head': 'Фамилия',
        'stored': True,
        'sortable': True,
        'filterable': True,
    },
    {
        'attribute': 'iname',
        'head': 'Имя',
        'stored': True,
        'sortable': True,
        'filterable': True,
    },
    {
        'attribute': 'oname',
        'head': 'Отчество',
        'stored': True,
        'sortable': True,
        'filterable': True,
    },
    {
        'attribute': 'passport_series',
        'head': 'Серия паспорта',
        'stored': False,
        'sortable': False,
        'filterable': True,
    },
    {
        'attribute': 'passport_number',
        'head': 'Номер паспорта',
        'stored': False,
        'sortable': True,
        'filterable': True,
    },
]

context = dict(
    head_title='Клиенты библиотеки',
    list_header='Список клиентов:',
    no_data_message='Не найдено ни одного клиента!',
    columns=columns
)

list_view = get_list_view(template_name, context)
get_js = get_js_getter(context)
get_rows = get_rows_getter(model, columns)
