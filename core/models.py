from django.db import models


class Person(models.Model):
    GENDERS = {
        'м': 'Мужской',
        'ж': 'Женский',
        '-': 'Не указан',
    }

    fname = models.CharField(max_length=50)
    iname = models.CharField(max_length=50)
    oname = models.CharField(max_length=50, null=True)
    gender = models.CharField(max_length=1, choices=GENDERS.items(), default='-')
    birthday = models.DateField(null=False, blank=False)

    class Meta:
        abstract = True

    def __str__(self):
        return ' '.join((self.iname, self.fname))

    def get_fio(self):

        return ' '.join(
            filter(None, (self.fname, self.iname, self.oname)))
