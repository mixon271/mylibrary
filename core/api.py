import json
from functools import partial
from collections import namedtuple
from operator import itemgetter

from django.shortcuts import render
from django.http import HttpResponse
from django.views.decorators.csrf import ensure_csrf_cookie

from core.views import context as core_context


def singleton(cls):
    instances = {}

    def get_instance():
        if cls not in instances:
            instances[cls] = cls()
        return instances[cls]

    return get_instance


Sorting = namedtuple('Sorting', 'param order')


def get_rows_getter(model, columns):

    def rows_getter(request):
        rows = get_rows(
            model, columns,
            get_filters(request),
            get_sorting(request)
        )
        return HttpResponse(json.dumps(rows))

    return rows_getter


def make_final_context(context):
    final_context = core_context.copy()
    final_context.update(context)
    return final_context


def get_list_view(template_name, context):

    @ensure_csrf_cookie
    def list_view(request):
        return render(request, template_name, make_final_context(context))

    return list_view


def get_js_getter(context):

    def get_js(request, script_name):
        return render(request, script_name, make_final_context(context))

    return get_js


def get_rows(model, columns, filters, sorting):
    stored_params = {
        c['attribute'] for c in columns if c['stored']
    }

    calculable_params = {
        c['attribute'] for c in columns if not c['stored']
    }
    if not filters:
        rows = model.objects.all()
        calculable_params = set()
    else:
        filter_kwargs = {
            param + '__icontains': filters[param]
            for param in stored_params if
            filters.get(param)
        }
        rows = model.objects.filter(**filter_kwargs)

    if sorting is not None and sorting.param in stored_params:
        if sorting.order == 'desc':
            sorting_param = '-'+sorting.param
        else:
            sorting_param = sorting.param
        rows = rows.order_by(sorting_param)

    row_data = [
        {
            'pk': row.pk,
            'data': {
                column['attribute']: (
                    deep_getattr(row, column['attribute'])
                    if column['stored'] else
                    getattr(row, 'get_'+column['attribute'])()
                )
                for column in columns
            }
        }
        for row in rows
    ]

    row_data = filter_calculables(
        row_data,
        {k: v for k, v in filters.items()
         if k in calculable_params}
    )

    if sorting is not None and sorting.param in calculable_params:
        row_data = sorting_calculables(row_data, sorting)

    result = {
        'row_data': row_data,
        'filters': filters,
        'sorting': {} if sorting is None else dict((sorting,)),
    }
    return result


def deep_getattr(obj, attribute):
    for attr in attribute.split('__'):
        obj = getattr(obj, attr, None)
    return obj


def get_filters(request):
    return {
        k[0:k.rfind('_')]: v
        for k, v in request.POST.items() if
        k.endswith('_filter')
    }


def filter_calculables(rows, filters):
    if not filters:
        return rows
    predicate = partial(is_suitable, filters=filters)
    return list(filter(predicate, rows))


def is_suitable(row, filters):
    return all((
        filters[k].lower() in v.lower()
        for k, v in row['data'].items() if k in filters
    ))


def get_sorting(request):
    try:
        key, value = next((
            (k, v) for k, v in request.POST.items()
            if k.endswith('_sorting')
        ))
        result = Sorting(key[0:key.rfind('_')], value)
    except StopIteration:
        result = None
    return result


def sorting_calculables(rows, sorting):
    return sorted(rows, key=lambda x: x['data'][sorting.param],
                  reverse=(sorting.order == 'desc'))
