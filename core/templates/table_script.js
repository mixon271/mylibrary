var up_arrow = '\u2191';
var down_arrow = '\u2193';
var arrows = [up_arrow, down_arrow];
var orders = {};
orders[up_arrow] = 'desc';
orders[down_arrow] = 'asc';
// var arrow_regexp = ['(', up_arrow, '|', down_arrow, ')'].join('');

function CreateRequest()
{
    var request = false;

    if (window.XMLHttpRequest)
    {
        request = new XMLHttpRequest();
    }

    if (!request)
    {
        alert('Не могу создать XMLHttpRequest!');
    }

    return request;
}

function SendRequest(method, url, args, handler)
{
    var request = CreateRequest();

    if (!request) return;

    request.onreadystatechange = function(){};
}

function GetTableRows()
{
    var filters = collect_filters();
    var sorting = get_sorting();

    var request = CreateRequest();
    if (!request) return;

    var url = window.location.href + 'rows/';
    var test_params = '';
    if (filters && sorting)
    {
        test_params = filters + '&' + sorting;
    }
    else if (filters && !sorting)
    {
        test_params = filters;
    }
    else if (!filters && sorting)
    {
        test_params = sorting;
    }

    request.onreadystatechange = function()
    {
        if (request.readyState == 4)
        {
            if (request.status == 200)
            {
                var data_block = document.getElementById('{{ data_block_id }}');

                if (!data_block)
                {
                    alert('Не задан элемент для отображения данных!');
                    return;
                }
                var response = JSON.parse(request.responseText);

                var table_text = '<table>';
                var element_id, is_readonly, filter_value, sorting_value;

                // Печать заголовка
                table_text += '<tr>';
                {% for column in columns %}
                    element_id = '{{ column.attribute }}' + '_header';
                    table_text += '<th id="' + element_id + '"';
                    if ({{ column.sortable|lower }})
                    {
                        table_text += ' onclick="make_sort(event)"';
                    }
                    table_text += '> {{ column.head }}'
                    sorting_value = response.sorting['{{ column.attribute }}']
                    if (sorting_value === 'asc')
                    {
                        table_text += down_arrow;
                    }
                    else if (sorting_value === 'desc')
                    {
                        table_text += up_arrow
                    }

                     table_text += ' </th>';
                {% endfor %}
                table_text += '</tr>';

                // фильтры
                table_text += '<tr>';
                {% for column in columns %}
                    element_id = '{{ column.attribute }}' + '_filter';
                    is_readonly = !{{ column.filterable|lower }};
                    filter_value = response.filters['{{ column.attribute }}'];
                    table_text += '<td><input id="' + element_id + '" type="text"';
                    if (is_readonly)
                    {
                        table_text += ' readonly="true"';
                    }

                    if (filter_value)
                    {
                        table_text += ' value="' + filter_value + '"';
                    }
                    table_text += ' onkeypress="on_enter_press(event)">'
                    table_text += '</td>';
                {% endfor %}
                table_text += '</tr>';

                // Печать данных
                var rows = response.row_data;
                var row_count = rows.length;

                for (var i = 0; i < row_count; ++i)
                {
                    table_text += '<tr>';
                    {% for column in columns %}
                        table_text += '<td>' + rows[i]['data'].{{ column.attribute }} + '</td>';
                    {% endfor %}
                    table_text += '</tr>';
                }

                table_text += '</table>';

                data_block.innerHTML = table_text;
            }
            else
            {
                console.log('Ajax-request is failured!');
            }
        }
        else
        {
            console.log('Ajax-request in process. . .');
        }
    }

    request.open('post', url, true);
    request.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded; charset=utf-8');
    var csrfCookie = document.cookie.match(/csrftoken=([\w-]+)/)[1];
    if (!!csrfCookie)
    {
        request.setRequestHeader('x-csrftoken', csrfCookie);
    }

    request.send(test_params);
}

function on_body_load()
{
    GetTableRows();
}

function collect_filters()
{
    var input_collection = document.getElementsByTagName('input');
    var input_array = [].slice.call(input_collection);
    var filter_array = input_array.filter(function(e){return e.id.endsWith('_filter')});
    var filter_params = filter_array.map(function(e){
        return [e.id, '=', encodeURIComponent(e.value)].join('');
    }).join('&');
    return filter_params;
}

function get_sorting()
{
    var i;
    var headers_collection = document.getElementsByTagName('th');
    var headers_array = [].slice.call(headers_collection);
    var last_char, param, param_name_end, order;

    var result = '';

    for (i = 0; i < headers_array.length; ++i)
    {
        last_char = headers_array[i].innerText.slice(-1);
        if (arrows.includes(last_char))
        {
            param_name_end = headers_array[i].id.lastIndexOf('_');
            param = headers_array[i].id.slice(0, param_name_end);
            result = param + '_sorting=' + orders[last_char];
        }
    }

    return result;
}

function on_enter_press(event)
{
    if (event.keyCode != 13) return;
    GetTableRows();
}

function make_sort(event)
{
    var i;
    var headers_collection = document.getElementsByTagName('th');
    var headers_array = [].slice.call(headers_collection);

    var clicked_element = event.target;
    var last_char = clicked_element.innerText.slice(-1);
    if (arrows.includes(last_char))
    {
        new_arrow = arrows[1 - arrows.indexOf(last_char)];
        clicked_element.innerText = clicked_element.innerText.replace(
            last_char, new_arrow);
    }
    else
    {
        for (i = 0; i < headers_array.length; ++i)
        {
            headers_array[i].innerText = headers_array[i].innerText.replace(
                /(\u2191|\u2193)/g, '');
        }
        clicked_element.innerText += down_arrow;
    }
    GetTableRows();
}