import json

from django.shortcuts import render
from django.views.decorators.csrf import ensure_csrf_cookie


context = {
    'data_block_id': 'data_block',
}


class CoreListView:
    model = None
    template_name = None

    head_title = 'Заголовок страницы'
    list_header = 'Заголовок таблицы'
    no_data_message = 'Нет данных!'

    # columns -- список словарей. Каждый словарь содержит следующую информацию:
    # 1. attribute -- имя отображаемого атрибута
    # 2. head -- заголовок столбца
    # 3. stored -- равно True, если значение атрибута хранится в БД, в
    #              противном случае атрибут является вычислимым
    # 4. sortable -- можно ли сортировать столбец
    # 5. filterable -- можно ли фильтровать значения
    #
    # Если поле не является хранимым, то модель должна предоствлять метод для
    # вычисления этого поля. При этом имя метода строится по правилу
    # 'get_' + column['attribute']
    columns = None

    table_id = 'data_table'

    def get_context(self):
        return {
            'head_title': self.head_title,
            'list_header': self.list_header,
            'no_data_message': self.no_data_message,
            'table_id': self.table_id,
            'columns_info': json.dumps(self.columns)
            # 'rows': self.get_rows(),
        }

    @ensure_csrf_cookie
    def get_rows(self, *args, **kwargs):
        rows = self.model.objects.all()
        print('In get_rows!')
        result = [
            {
                column['attribute']: (
                    getattr(row, column['attribute'])
                    if column['stored'] else
                    getattr(row, 'get_' + column['attribute'])()
                )
                for row in rows
            }
            for column in self.columns
        ]
        return HttpResponse(json.dumps(result))

    @ensure_csrf_cookie
    def get_js(self, request, script_name):
        return render(
            request,
            script_name,
            self.get_context()
        )

    # @ensure_csrf_cookie
    def render(self, request, *args):
        return render(
            request,
            self.template_name,
            self.get_context()
        )
