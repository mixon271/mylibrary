from core.api import singleton
from core.views import CoreListView
from books.models import Book, Author, BookExemplar


@singleton
class BookListView(CoreListView):
    model = Book
    template_name = 'book_list.html'

    head_title = 'Книги'
    list_header = 'Список книг'
    no_data_message = 'Не найдено ни одной книги!'

    def get_rows(self):
        rows = super().get_rows()
        for row in rows:
            row.authors_enum = row.get_authors_enum()
        return rows


@singleton
class AuthorListView(CoreListView):
    model = Author
    template_name = 'authors_list.html'

    head_title = 'Авторы'
    list_header = 'Список авторов'

    def get_rows(self):
        rows = super().get_rows()
        for row in rows:
            row.fio = row.get_fio()
        return rows


@singleton
class ExemplarListView(CoreListView):
    model = BookExemplar
    template_name = 'exemplar_list.html'

    head_title = 'Книги в наличии'
    list_header = 'В библиотеке вы можете взять следующие книги'
