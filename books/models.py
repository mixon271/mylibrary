from django.db import models

from core.models import Person


class Author(Person):
    """
    Автор книг.

    Думаю, тут всё очевидно.
    """
    deathday = models.DateField(null=True)
    info = models.TextField()

    class Meta:
        db_table = 'authors'


class Book(models.Model):
    """
    Модель книги.

    Под книгой подразумевается произведение автора, а не какой-либо конкретный
    экземпляр.

    Данные о конкретных экземплярах книг хранятся в модели BookExemplar.
    """

    # Один автор может написать много книг, и в то же время одна книга может
    # быть написана несколькими авторами. Поэтому между моделями авторов и книг
    # связь много-ко-многому
    authors = models.ManyToManyField(Author, related_name='books')
    name = models.CharField(max_length=500)

    class Meta:
        db_table = 'books'

    def __str__(self):
        return self.name

    def get_authors_enum(self):
        return ', '.join(
            map(str, self.authors.all()))


class Storage(models.Model):
    code = models.CharField(max_length=10)
    capacity = models.PositiveSmallIntegerField()

    city = models.CharField(max_length=100)
    street = models.CharField(max_length=100)
    house = models.PositiveSmallIntegerField()

    class Meta:
        db_table = 'storages'

    def __str__(self):
        return self.code


class BookExemplar(models.Model):
    """
    Модель экземпляра книги.

    Хранит информацию о конкретных экземплярах: издательство, год издания, а
    также информацию о месте хранения книги.

    Информацию о названии и об авторе (авторах) можно получить по ссылке на
    модель Book.
    """
    book = models.ForeignKey(Book, related_name='exemplars')
    publishing_year = models.PositiveSmallIntegerField()
    publishing_house = models.CharField(max_length=50)
    storage = models.ForeignKey(Storage, related_name='storable')

    class Meta:
        db_table = 'exemplars'

    def __str__(self):
        return ' '.join((str(self.book), self.publishing_year))
