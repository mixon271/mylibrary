from django.conf.urls import url

import books.views as views

urlpatterns = [
    url(r'^$', views.BookListView().render),
    url(r'^authors$', views.AuthorListView().render),
    url(r'^exemplars$', views.ExemplarListView().render),
]